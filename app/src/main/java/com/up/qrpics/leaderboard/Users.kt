package com.up.qrpics.leaderboard

import com.up.qrpics.model.AnsweredQuestion
import com.up.qrpics.model.User

class Users {

    fun getUsers(): List<User> {
        val tmp: MutableList<User> = mutableListOf()

        User(
            "1",
            "marjusz",
            imgUrl,
            getAnsweredQuestions1().toMutableList()
        )

        return tmp.apply {
            add(
                User(
                    "1",
                    "marjusz",
                    imgUrl,
                    getAnsweredQuestions1().toMutableList()
                )
            )

            add(
                User(
                    "2",
                    "krysjtan",
                    imgUrl,
                    getAnsweredQuestions2().toMutableList()
                )
            )

            add(
                User(
                    "3",
                    "karyna",
                    imgUrl,
                    getAnsweredQuestions3().toMutableList()
                )
            )

            add(
                User(
                    "4",
                    "dzesika",
                    imgUrl,
                    getAnsweredQuestions4().toMutableList()
                )
            )
        }

    }

    fun getAnsweredQuestions1(): List<AnsweredQuestion> {
        val tmp: MutableList<AnsweredQuestion> = mutableListOf()

        AnsweredQuestion(
            "1",
            "what is the color of sky",
            "4",
            "4"
        )

        return tmp.apply {
            add(
                AnsweredQuestion(
                    "1",
                    "what is the color of sky",
                    "4",
                    "4"
                )
            )

            add(
                AnsweredQuestion(
                    "2",
                    "asdas dasd??",
                    "4",
                    "4"
                )
            )

            add(
                AnsweredQuestion(
                    "3",
                    "dakdjal askjdla skdja ls???",
                    "3",
                    "3"
                )
            )


            add(
                AnsweredQuestion(
                    "4",
                    "adsd s ddkkdd???",
                    "1",
                    "1"
                )
            )

        }

    }

    fun getAnsweredQuestions2(): List<AnsweredQuestion> {
        val tmp: MutableList<AnsweredQuestion> = mutableListOf()

        return tmp.apply {
            add(
                AnsweredQuestion(
                    "1",
                    "what is the color of sky",
                    "4",
                    "2"
                )
            )

            add(
                AnsweredQuestion(
                    "2",
                    "asdas dasd??",
                    "4",
                    "1"
                )
            )

            add(
                AnsweredQuestion(
                    "3",
                    "dakdjal askjdla skdja ls???",
                    "3",
                    "1"
                )
            )


            add(
                AnsweredQuestion(
                    "4",
                    "adsd s ddkkdd???",
                    "1",
                    "4"
                )
            )

        }

    }


    fun getAnsweredQuestions3(): List<AnsweredQuestion> {
        val tmp: MutableList<AnsweredQuestion> = mutableListOf()

        return tmp.apply {
            add(
                AnsweredQuestion(
                    "1",
                    "what is the color of sky",
                    "4",
                    "2"
                )
            )

            add(
                AnsweredQuestion(
                    "2",
                    "asdas dasd??",
                    "4",
                    "1"
                )
            )

            add(
                AnsweredQuestion(
                    "3",
                    "dakdjal askjdla skdja ls???",
                    "3",
                    "3"
                )
            )


            add(
                AnsweredQuestion(
                    "4",
                    "adsd s ddkkdd???",
                    "1",
                    "1"
                )
            )

        }

    }


    fun getAnsweredQuestions4(): List<AnsweredQuestion> {
        val tmp: MutableList<AnsweredQuestion> = mutableListOf()

        return tmp.apply {
            add(
                AnsweredQuestion(
                    "1",
                    "what is the color of sky",
                    "4",
                    "2"
                )
            )

            add(
                AnsweredQuestion(
                    "2",
                    "asdas dasd??",
                    "4",
                    "4"
                )
            )

            add(
                AnsweredQuestion(
                    "3",
                    "dakdjal askjdla skdja ls???",
                    "3",
                    "3"
                )
            )


            add(
                AnsweredQuestion(
                    "4",
                    "adsd s ddkkdd???",
                    "1",
                    "1"
                )
            )

        }

    }

    companion object {
        const val imgUrl =
            "https://lh3.googleusercontent.com/a-/AAuE7mDgo0pjzPvmeN7VkW6bQSDvzNTE1J1UckNUBOBG"
    }
}