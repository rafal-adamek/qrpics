package com.up.qrpics.leaderboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.up.qrpics.R
import de.hdodenhof.circleimageview.CircleImageView


class BoardAdapter(
    private val boardRecords: List<BoardRecord>
) : RecyclerView.Adapter<BoardAdapter.VH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.boardposition,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = boardRecords.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(boardRecords[position], position)
    }

    class VH(private val v: View) : RecyclerView.ViewHolder(v) {
        private val root: ConstraintLayout = v.findViewById(R.id.root)
        private val position: TextView = v.findViewById(R.id.position)
        private val name: TextView = v.findViewById(R.id.name)
        private val points: TextView = v.findViewById(R.id.points)
        private val img : CircleImageView = v.findViewById(R.id.profile_image)

        fun bind(boardRecords: BoardRecord, pos: Int) {
            name.text = boardRecords.name
            points.text = "${(boardRecords.points)} pts."
            position.text = (pos + 1).toString()

            boardRecords.url?.let {
                Glide.with(v)
                    .load(it)
                    .into(img)
            }

            changeTextColor(ContextCompat.getColor(v.context, R.color.white))
            val color: Int
            when (pos) {
                0 -> {
                    color = R.color.firstPlace
                }
                1 -> {

                    color = R.color.secondPlace
                }
                2 -> {
                    color = R.color.thirdPlace
                }
                else -> {
                    changeTextColor(ContextCompat.getColor(v.context, R.color.otherPlacesTextColor))
                    color = R.color.otherPlaces
                }
            }
            root.setBackgroundResource(color)

        }

        private fun changeTextColor(color: Int) {
            name.setTextColor(color)
            points.setTextColor(color)
            position.setTextColor(color)
        }
    }
}

