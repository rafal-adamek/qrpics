package com.up.qrpics.leaderboard

data class BoardRecord(
    val name: String,
    val points: Int,
    val url : String?
)