package com.up.qrpics.leaderboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.up.qrpics.R
import com.up.qrpics.model.User
import com.up.qrpics.repository.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_leaderboard.*
import org.koin.android.ext.android.inject

class LeaderboardActivity : AppCompatActivity() {

    private val dataRepository: DataRepository by inject()
    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)

        disposable += dataRepository
            .getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                rv.adapter = BoardAdapter(UsersToBoardRowsMapper.map(it))
            },onError =  { Log.e("LeaderboardActivity","Error v",it)})


    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    companion object {
        fun getIntent(
            context: Context
        ) = Intent(context, LeaderboardActivity::class.java)
    }
}

object UsersToBoardRowsMapper {
    private const val QUESTION_FOUND_POINTS = 1
    private const val GOOD_ANSWER_POINTS = 3
    fun map(users: List<User>): List<BoardRecord> {
        val boardRecords: MutableList<BoardRecord> = mutableListOf()

        users.forEach { user ->
            var points = 0
            user.questions.forEach { answeredQuestion ->
                points += if (answeredQuestion.properAnswerId == answeredQuestion.userAnswerId) {
                    GOOD_ANSWER_POINTS
                } else {
                    QUESTION_FOUND_POINTS
                }

            }
            boardRecords.add(BoardRecord(user.nickname, points, user.photoUrl))
        }
        return boardRecords.sortedBy { it.points }.reversed()
    }
}

