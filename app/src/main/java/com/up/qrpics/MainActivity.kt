package com.up.qrpics

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.up.qrpics.repository.DataRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    private val disposable = CompositeDisposable()
    private val dataRepository: DataRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        disposable += dataRepository
            .getQuestion("LOfvxnZOBKO6VhkbYtVZ")
            .subscribe({
                Log.d(TAG, "Question fetched $it")
            }, {})
        disposable += dataRepository
            .getUsers()
            .subscribe(
                { users ->
                    Log.d(TAG, "Having users! $users")
                },
                {

                })
    }
}
