package com.up.qrpics.repository

import com.up.qrpics.model.Question
import com.up.qrpics.model.User
import io.reactivex.Completable
import io.reactivex.Single

interface DataRepository {
    fun getQuestion(id: String): Single<Question>
    fun setAnswer(
        userId: String,
        question: String,
        questionId: String,
        properAnswerId: String,
        userAnswerId: String
    ): Completable

    fun setUserData(id: String, nickname: String, photoUrl: String?)
    fun getUsers(): Single<List<User>>
}