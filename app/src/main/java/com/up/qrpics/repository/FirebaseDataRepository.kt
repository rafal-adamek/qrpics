package com.up.qrpics.repository

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.up.qrpics.model.Answer
import com.up.qrpics.model.AnsweredQuestion
import com.up.qrpics.model.Question
import com.up.qrpics.model.User
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles

class FirebaseDataRepository(private val firestore: FirebaseFirestore) : DataRepository {

    private val TAG = "FirebaseDataRepository"

    override fun getQuestion(id: String): Single<Question> {
        return Singles.zip(
            firestore
                .collection("questions")
                .document(id)
                .get()
                .toSingle(),
            firestore
                .collection("questions")
                .document(id)
                .collection("answers")
                .get()
                .toSingle()
        ) { question, answers ->
            Question(
                question.id,
                question.getString("question")!!,
                answers
                    .documents
                    .map {
                        Answer(
                            it.id,
                            it.getString("answer")!!
                        )
                    }.toMutableList(),

                question.getString("properAnswerId")!!
            )
        }

//        return Single.just(
//            Question(
//                "0",
//                "In which container should plastic go?",
//                listOf(Answer("0", "Blue"), Answer("1", "Yellow"), Answer("2", "Green")),
//                "1"
//            )
//        )
    }

    override fun setUserData(id: String, nickname: String, photoUrl: String?) {
        Log.d(TAG, "setUserData $id, $nickname $photoUrl")

        val data = hashMapOf(
            "nickname" to nickname,
            "photoUrl" to photoUrl
        )

        firestore
            .collection("users")
            .document(id)
            .set(data, SetOptions.merge())
    }

    override fun setAnswer(
        userId: String,
        question: String,
        questionId: String,
        properAnswerId: String,
        userAnswerId: String
    ): Completable {
        val data = hashMapOf(
            "properAnswerId" to properAnswerId,
            "question" to question,
            "userAnswerId" to userAnswerId
        )

        return Completable.fromCallable {
            firestore
                .collection("users")
                .document(userId)
                .collection("answeredQuestions")
                .document(questionId)
                .set(data, SetOptions.merge())
        }
    }

    override fun getUsers(): Single<List<User>> {
        return firestore
            .collection("users")
            .get()
            .toSingle()
            .toObservable()
            .flatMapIterable { it.documents }
            .flatMapSingle { user ->
                user
                    .reference
                    .collection("answeredQuestions")
                    .get()
                    .toSingle()
                    .map { user to it }
            }.map<User> { data ->
                User(
                    data.first.id,
                    data.first.getString("nickname")!!,
                    data.first.getString("photoUrl"),
                    data
                        .second
                        .documents
                        .map {
                            AnsweredQuestion(
                                it.id,
                                it.getString("question")!!,
                                it.getString("properAnswerId")!!,
                                it.getString("userAnswerId")!!
                            )
                        }
                        .toMutableList()

                )
            }
            .toList()

    }


    private inline fun <reified T> Task<T>.toSingle(): Single<T> {
        val task = this
        return Single.create { emitter ->
            task
                .addOnSuccessListener {
                    emitter.onSuccess(it)
                }
                .addOnFailureListener {
                    emitter.onError(it)
                }
        }
    }

}
