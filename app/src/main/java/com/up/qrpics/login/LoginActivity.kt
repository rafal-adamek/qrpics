package com.up.qrpics.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.up.qrpics.R
import com.up.qrpics.arcore.AugmentedImageActivity
import com.up.qrpics.repository.DataRepository
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject


class LoginActivity : AppCompatActivity() {

    private val googleClient: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(
            this,
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        )
    }

    private val dataRepository: DataRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        GoogleSignIn.getLastSignedInAccount(this)?.let(::loggedInFlow) ?: signInFlow()
    }

    private fun signInFlow() {
        with(signin) {
            visibility = View.VISIBLE
            setOnClickListener { signIn() }
        }
    }

    private fun loggedInFlow(account: GoogleSignInAccount) {
        dataRepository.setUserData(
            account.id!!,
            account.displayName!!,
            account.photoUrl?.toString()
        )
        startActivity(Intent(this, AugmentedImageActivity::class.java).apply {
            putExtra(KEY_LOGIN_ID, account.id)
        })
        finish()
    }


    private fun signIn() {
        startActivityForResult(googleClient.signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_SIGN_IN -> handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            completedTask.getResult(ApiException::class.java)?.let(::loggedInFlow)
        } catch (e: ApiException) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }

    }

    companion object {
        const val RC_SIGN_IN = 1
        const val KEY_LOGIN_ID = "key_login_id"
    }

}
