package com.up.qrpics.form

import android.os.Bundle
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.up.qrpics.R
import com.up.qrpics.model.Answer
import com.up.qrpics.model.Question
import com.up.qrpics.repository.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_form.*
import org.koin.android.ext.android.inject

class FormActivity : AppCompatActivity() {

    private val repository: DataRepository by inject()

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_form)

        intent.extras?.getString(KEY_QUESTION_ID)?.let(::fetchQuestion)
            ?: throw IllegalStateException("No question id passed")
    }

    private fun fetchQuestion(questionId: String) {
        repository.getQuestion(questionId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::render)
            .let(disposables::add)
    }

    private fun render(question: Question) {
        questionText.text = question.question

        answersGroup.apply {
            for (answer: Answer in question.answers) {
                RadioButton(this@FormActivity).let {
                    it.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    it.id = answer.id.hashCode()
                    it.text = answer.answer
                    it.tag = answer.id
                    addView(it)
                }
            }
        }

        positiveButton.setOnClickListener {
            repository.setAnswer(
                userId = GoogleSignIn.getLastSignedInAccount(this)?.id ?: "Nope",
                question = question.question,
                questionId = question.id,
                properAnswerId = question.properAnswerId,
                userAnswerId = answersGroup.findViewById<RadioButton>(answersGroup.checkedRadioButtonId).tag as String
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    finish()
                }, {
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                })
        }

    }

    companion object {
        const val KEY_QUESTION_ID = "key_question_id"
    }
}