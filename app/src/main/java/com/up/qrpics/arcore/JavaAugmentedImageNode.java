package com.up.qrpics.arcore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.up.qrpics.R;
import com.up.qrpics.repository.DataRepository;

import java.util.concurrent.CompletableFuture;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Node for rendering an augmented image. The image is framed by placing the virtual picture frame
 * at the corners of the augmented image trackable.
 */
@SuppressWarnings({"AndroidApiChecker"})
public class JavaAugmentedImageNode extends AnchorNode {

    private static final String TAG = "AugmentedImageNode";

    // The augmented image represented by this node.
    private AugmentedImage image;

    // Models of the 4 corners.  We use completable futures here to simplify
    // the error handling and asynchronous loading.  The loading is started with the
    // first construction of an instance, and then used when the image is set.
    static CompletableFuture<ViewRenderable> ulCorner;

    private Context context;
    private PublishSubject<String> publishSubject;
    private DataRepository repository;

    public JavaAugmentedImageNode(Context context, PublishSubject<String> publishSubject, DataRepository repository) {
        this.context = context;
        this.publishSubject = publishSubject;
        this.repository = repository;
        // Upon construction, start loading the models for the corners of the frame.
        if (ulCorner == null) {
            ulCorner = ViewRenderable.builder()
                    .setView(context, R.layout.renderable_view)
                    .build();
        }
    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image. The corners are then positioned based on the
     * extents of the image. There is no need to worry about world coordinates since everything is
     * relative to the center of the image, which is the parent node of the corners.
     */
    @SuppressLint("CheckResult")
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image) {
        this.image = image;

        // If any of the models are not loaded, then recurse when all are loaded.
        if (!ulCorner.isDone()) {
            ulCorner.thenAccept((viewRenderable -> {
                setImage(image);
                TextView view = viewRenderable.getView().findViewById(R.id.cloud_text);
                repository.getUsers()
                        .flatMapObservable(users -> Observable.fromIterable(users))
//                        .filter(user -> user.getId().equals(GoogleSignIn.getLastSignedInAccount(context).getId()))
                        .firstOrError()
                        .map(user -> user.getQuestions())
                        .flatMapObservable(questions -> Observable.fromIterable(questions))
                        .filter(question -> question.getId().equals(image.getName()))
                        .firstOrError()
                        .subscribe(success -> {
                            if(success.getUserAnswerId().equals(success.getProperAnswerId())) {
                                view.setText(R.string.correct_question);
                            } else {
                                view.setText(R.string.no_correct_question);
                            }
                            view.setOnClickListener(null);
                        }, error -> {
                            view.setText(R.string.no_question);
                            view.setOnClickListener(view1 -> publishSubject.onNext(image.getName()) );
                        });



            }))
                    .exceptionally(
                            throwable -> {
                                Log.e(TAG, "Exception loading", throwable);
                                return null;
                            });
        }

        // Set the anchor based on the center of the image.
        setAnchor(image.createAnchor(image.getCenterPose()));

        Vector3 localPosition = new Vector3();
        Node cornerNode;

        // Upper left corner.
        localPosition.set(-0.5f * image.getExtentX(), 0.0f, -0.5f * image.getExtentZ());
        cornerNode = new Node();
        cornerNode.setParent(this);
        cornerNode.setLocalPosition(localPosition);
        cornerNode.setLocalRotation(Quaternion.eulerAngles(new Vector3(270f, 0f, 0f)));
        ViewRenderable renderable = ulCorner.getNow(null);
        cornerNode.setRenderable(renderable);
    }

    public AugmentedImage getImage() {
        return image;
    }
}

