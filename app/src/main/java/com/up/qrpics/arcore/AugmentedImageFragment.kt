package com.up.qrpics.arcore

import android.app.ActivityManager
import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.up.qrpics.arcore.hax.HaxArFragment
import com.up.qrpics.utils.SnackbarHelper
import java.io.IOException

class AugmentedImageFragment : HaxArFragment() {
    private val TAG = "AugmentedImageFragment"


    private val imageMap = mapOf(
        "LOfvxnZOBKO6VhkbYtVZ" to "recycling.png"
    )

    // This is the name of the image in the sample database.  A copy of the image is in the assets
    // directory.  Opening this image on your computer is a good quick way to test the augmented image
    // matching.
    private val DEFAULT_IMAGE_NAME_2 = "recycling.png"

    // This is a pre-created database containing the sample image.
    private val SAMPLE_IMAGE_DATABASE = "sample_database.imgdb"

    // Augmented image configuration and rendering.
    // Load a single image (true) or a pre-generated image database (false).
    private val USE_SINGLE_IMAGE = true

    // Do a runtime check for the OpenGL level available at runtime to avoid Sceneform crashing the
    // application.
    private val MIN_OPENGL_VERSION = 3.0


    override fun onAttach(context: Context) {
        super.onAttach(context)

        // Check for Sceneform being supported on this device.  This check will be integrated into
        // Sceneform eventually.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later")
            SnackbarHelper.getInstance()
                .showError(activity, "Sceneform requires Android N or later")
        }

        val openGlVersionString =
            (context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .deviceConfigurationInfo
                .glEsVersion
        if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 or later")
            SnackbarHelper.getInstance()
                .showError(activity, "Sceneform requires OpenGL ES 3.0 or later")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        // Turn off the plane discovery since we're only looking for images
        planeDiscoveryController.hide()
        planeDiscoveryController.setInstructionView(null)
        arSceneView.planeRenderer.isEnabled = false
        return view
    }

    override fun getSessionConfiguration(session: Session): Config {
        val config = super.getSessionConfiguration(session)
        if (!setupAugmentedImageDatabase(config, session)) {
            SnackbarHelper.getInstance()
                .showError(activity, "Could not setup augmented image database")
        }
        return config
    }

    private fun setupAugmentedImageDatabase(config: Config, session: Session): Boolean {
        var augmentedImageDatabase: AugmentedImageDatabase? = null

        val assetManager = if (context != null) context!!.assets else null
        if (assetManager == null) {
            Log.e(TAG, "Context is null, cannot intitialize image database.")
            return false
        }

        // There are two ways to configure an AugmentedImageDatabase:
        // 1. Add Bitmap to DB directly
        // 2. Load a pre-built AugmentedImageDatabase
        // Option 2) has
        // * shorter setup time
        // * doesn't require images to be packaged in apk.
        if (USE_SINGLE_IMAGE) {
            val augmentedImageBitmap = loadAugmentedImageBitmap(assetManager)
            if (augmentedImageBitmap.isEmpty()) {
                return false
            }

            augmentedImageDatabase = AugmentedImageDatabase(session)
            for (dbImage in augmentedImageBitmap) {
                augmentedImageDatabase.addImage(dbImage.id, dbImage.bitmap)
            }
            // If the physical size of the image is known, you can instead use:
            //     augmentedImageDatabase.addImage("image_name", augmentedImageBitmap, widthInMeters);
            // This will improve the initial detection speed. ARCore will still actively estimate the
            // physical size of the image as it is viewed from multiple viewpoints.
        } else {
            // This is an alternative way to initialize an AugmentedImageDatabase instance,
            // load a pre-existing augmented image database.
            try {
                context!!.assets.open(SAMPLE_IMAGE_DATABASE).use { `is` ->
                    augmentedImageDatabase = AugmentedImageDatabase.deserialize(session, `is`)
                }
            } catch (e: IOException) {
                Log.e(TAG, "IO exception loading augmented image database.", e)
                return false
            }

        }

        config.augmentedImageDatabase = augmentedImageDatabase
        return true
    }


    private fun loadAugmentedImageBitmap(assetManager: AssetManager): List<DbImage.Image> {
        return imageMap.map {
            assetManager.open(it.value).use { inputStream ->
                DbImage.Image(it.key, it.value, BitmapFactory.decodeStream(inputStream))
            }
        }


    }

    sealed class DbImage {
        data class Image(
            val id: String,
            val fileName: String,
            val bitmap: Bitmap
        ) : DbImage()

        object Invalid : DbImage()
    }
}