package com.up.qrpics.arcore

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.ar.core.AugmentedImage
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.FrameTime
import com.up.qrpics.R
import com.up.qrpics.arcore.hax.HaxArFragment
import com.up.qrpics.form.FormActivity
import com.up.qrpics.leaderboard.LeaderboardActivity
import com.up.qrpics.repository.DataRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_cat_scanner.*
import org.koin.android.ext.android.inject
import java.util.*

class AugmentedImageActivity : AppCompatActivity() {

    private val TAG = "AugmentedImageActivity"

    private var arFragment: HaxArFragment? = null

    private val subject: PublishSubject<String> by inject()
    private val repository: DataRepository by inject()
    private val compositeDisposable = CompositeDisposable()

    // Augmented image and its associated center pose anchor, keyed by the augmented image in
    // the database.
    private val augmentedImageMap = HashMap<AugmentedImage, JavaAugmentedImageNode>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_scanner)

        arFragment = supportFragmentManager.findFragmentById(R.id.ux_fragment) as HaxArFragment?

        arFragment!!.arSceneView.scene.addOnUpdateListener {
            this.onUpdateFrame(
                it
            )
        }

        leaderboard.setOnClickListener {
            startActivity(LeaderboardActivity.getIntent(this))
        }

        compositeDisposable += subject.subscribe {
            Log.d(TAG, "Click!")
            startActivity(Intent(this, FormActivity::class.java).apply { putExtra(FormActivity.KEY_QUESTION_ID, it) })
        }
    }

    override fun onPause() {
        super.onPause()
        JavaAugmentedImageNode.ulCorner = null
        augmentedImageMap.forEach {
            arFragment!!.arSceneView.scene.removeChild(it.value)
        }
        augmentedImageMap.clear()
    }




    /**
     * Registered with the Sceneform Scene object, this method is called at the start of each frame.
     *
     * @param frameTime - time since last frame.
     */
    private fun onUpdateFrame(frameTime: FrameTime) {
        val frame = arFragment!!.arSceneView.arFrame ?: return

        // If there is no frame, just return.

        val updatedAugmentedImages = frame.getUpdatedTrackables(AugmentedImage::class.java)
        for (augmentedImage in updatedAugmentedImages) {
            when (augmentedImage.trackingState) {
                TrackingState.PAUSED -> {
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    val text = "Detected Image " + augmentedImage.index + " ${augmentedImage.name}"
                    Log.d(TAG, text)
                }

                TrackingState.TRACKING -> {
                    // Have to switch to UI Thread to update View.

                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        val node = JavaAugmentedImageNode(this, subject, repository)
                        node.setImage(augmentedImage)
                        augmentedImageMap[augmentedImage] = node
                        arFragment!!.arSceneView.scene.addChild(node)
                    }
                }

                TrackingState.STOPPED -> augmentedImageMap.remove(augmentedImage)
            }
        }
    }

}