package com.up.qrpics.model

data class Answer(
    val id: String,
    val answer: String
)

