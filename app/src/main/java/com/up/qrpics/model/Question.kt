package com.up.qrpics.model

data class Question(
    val id: String,
    val question: String,
    val answers: List<Answer>,
    val properAnswerId: String
)