package com.up.qrpics.model

data class AnsweredQuestion(
    val id: String,
    val question: String,
    val properAnswerId: String,
    val userAnswerId: String
)