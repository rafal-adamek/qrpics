package com.up.qrpics.model

data class User(
    val id: String,
    val nickname: String,
    val photoUrl: String?,
    val questions: MutableList<AnsweredQuestion> = mutableListOf()
)