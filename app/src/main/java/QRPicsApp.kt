package com.up.qrpics

import android.app.Application
import com.google.firebase.firestore.FirebaseFirestore
import com.up.qrpics.repository.DataRepository
import com.up.qrpics.repository.FirebaseDataRepository
import io.reactivex.subjects.PublishSubject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class QRPicsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@QRPicsApp)
            modules(appModule)
        }
    }

    private val appModule = module {
        single<DataRepository> { FirebaseDataRepository(get()) }
        single<FirebaseFirestore> { FirebaseFirestore.getInstance() }

        single<PublishSubject<String>> { PublishSubject.create() }
    }
}